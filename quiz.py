import json
import random


class Quiz:

    def __init__(self, kap=21):
        self.questions = [[] for __ in range(kap)]
        self.get_questions()
        self.start_meny()

    def add_single_question(self, spm, svar, kap):
        found = False
        kap = int(kap)
        d = 0
        for d in self.questions:
            for kap in d["kapittler"]:
                if kap["kap_nr"] == kap:
                    if spm in kap.values():
                        kap["spm"].append({"spm": spm, "svar": svar})
                    found = True
                    break
            if found:
                break

    def get_questions(self):
        with open("introspm.json", "r", encoding="UTF-8") as fil:
            self.questions = json.load(fil)

    @staticmethod
    def chomp(s):
        if s.endswith("\r\n"):
            return s[:-2]
        if s.endswith("\n"):
            return s[:-1]
        return s

    def new_question(self):
        spm = input("Skriv inn spørmål: ")
        svar = input("Skriv inn svaret på spørsmålet: ")
        kap = input("Hvilket kapittel hører spørsmålet til")
        self.add_single_question(spm, svar, kap)

    def save_values(self):
        with open("introspm.json", "w", encoding="UTF-8") as fil:
            json.dump(self.questions, fil)

    def start_quiz(self):
        questions = self.questions.copy()
        random.shuffle(questions)
        for d in questions:
            print("Del: " + str(d["del_nr"]) + ", Navn: " + d["del_navn"])
            kapittler = d["kapittler"].copy()
            random.shuffle(kapittler)
            for kap in kapittler:
                kap_q = kap["spm"].copy()
                random.shuffle(kap_q)
                print("Kapittel " + str(kap["kap_nr"]) + " om: " + kap["kap_navn"])
                for n, q in enumerate(kap_q, start=1):
                    print(n, q["spm"])
                    input()
                    print(q["svar"], end="\n\n")

    def get_questions_from_txt(self):
        with open("INTROSPOR.TXT", "r", encoding="UTF-8") as fil:
            fil_liste = fil.readlines()
            fil_liste = [self.chomp(f) for f in fil_liste]
            spm_liste = [fil_liste[i:i+3] for i in range(0, len(fil_liste), 3)]
            for spm in spm_liste:
                self.add_single_question(*spm)

    def print_all(self):
        for d in self.questions:
            print(d["del_navn"])
            for k in d["kapittler"]:
                print('\t', k["kap_nr"], k["kap_navn"])
                for qna in k["spm"]:
                    print('\t\t', qna["spm"])
                    print('\t\t', qna["svar"])

    def print_fmt(self):
        count = 0
        for d in self.questions:
            print(d["del_navn"])
            for k in d["kapittler"]:
                print('\t', k["kap_nr"], k["kap_navn"])
                for i, qna in enumerate(k["spm"], start=1):
                    print('\t\t', "Spm: ", i)
                    count += 1
        print("Antall spørsmål:", count)

    def print_options(self, valg):
        print("Dine valg er: ")
        for x in valg:
            print("Tegn: " + x["char"] + " , valg: " + x["value"])

    def start_meny(self):
        valg = [
            {"char": "o", "value": "Valgene dine", "action": self.print_options},
            {"char": "n", "value": "Nytt spørsmål", "action": self.new_question},
            {"char": "t", "value": "Legg til fra TXT", "action": self.get_questions_from_txt},
            {"char": "s", "value": "Start Quiz!", "action": self.start_quiz},
            {"char": "p", "value": "Print all Questions", "action": self.print_all},
            {"char": "l", "value": "Print fmt Questions", "action": self.print_fmt},
            {"char": "q", "value": "Slutt", "action": self.save_values},
        ]
        valid_cmd = [x["char"] for x in valg]
        while True:
            try:
                sv = input("Hva vil du? ").lower()
                if sv not in valid_cmd:
                    print("Ugyldig kommando (skirv o for valg)")
                    continue
                else:
                    if sv == "o":
                        valg[valid_cmd.index(sv)]["action"](valg)
                    else:
                        valg[valid_cmd.index(sv)]["action"]()
                        if sv == "q":
                            break
            except KeyboardInterrupt:
                self.save_values()
                break


if __name__ == "__main__":  # fungerer som main i cpp
    Quiz()
